FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM java:8
VOLUME /tmp
EXPOSE 8080
COPY --from=build /home/gradle/src/build/libs/*.jar ./book-manager-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","book-manager-1.0-SNAPSHOT.jar"]
